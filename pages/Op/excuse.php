<?php
$ipaddress = '';
    if (getenv('HTTP_CLIENT_IP')){
		$ipaddress = getenv('HTTP_CLIENT_IP');
	}else if(getenv('HTTP_X_FORWARDED_FOR')){
		$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	}else if(getenv('HTTP_X_FORWARDED')){
		$ipaddress = getenv('HTTP_X_FORWARDED');
	}else if(getenv('HTTP_FORWARDED_FOR')){
		$ipaddress = getenv('HTTP_FORWARDED_FOR');
	}else if(getenv('HTTP_FORWARDED')){
		$ipaddress = getenv('HTTP_FORWARDED');
	}else if(getenv('REMOTE_ADDR')){
		$ipaddress = getenv('REMOTE_ADDR');
    }else{
        $ipaddress = 'UNKNOWN';
	}
Include('../../config/connect223.php');
    header('Content-type: application/json');
    if(isset($_POST['get_in']) && !empty($_POST['get_in'])) {
    	
        $user_id =  $_POST['user_id'];
		$tanggal_absen =  $_POST['tanggal_absen'];

    	$sqlchekin = "select COUNT(USER_ID) AS CNT from SIAK_ABSENSI WHERE USER_ID = '$user_id' and to_char(TANGGAL,'DD/MM/YYYY') = '$tanggal_absen'";
		$resultchekin = oci_parse($conn222, $sqlchekin);
		oci_execute($resultchekin);
		$countchekin = oci_fetch_array($resultchekin);
		$do_check = $countchekin['CNT'];
		if($do_check > 0){
			$sql= "select  JAM_MASUK, JAM_KELUAR from SIAK_ABSENSI WHERE USER_ID = '$user_id' and to_char(TANGGAL,'DD/MM/YYYY') = '$tanggal_absen'";
			$result=oci_parse($conn222, $sql);
			oci_execute($result);
            $row = oci_fetch_array($result);
            $clock_in = $row['JAM_MASUK'];
            $clock_out = $row['JAM_KELUAR'];
			$output = array(
    			"message_type"=>1,
                "clock_in"=> $clock_in,
    			"clock_out"=> $clock_out,
                "message"=> $sqlchekin,
    			"sql"=> $sql
    		);
		}else{
			$output = array(
    			"message_type"=>2,
    			"clock_in"=>"00:00",
                "clock_out"=> "00:00",
    			"message"=>"Maaf Pada Tanggal $tanggal_absen Anda Tidak Clock-In Atau Clock Out",
                "sql"=> "SQL"  
    		);
		}
    	
        echo json_encode($output);
    }
     if(isset($_POST['get_date']) && !empty($_POST['get_date'])) {

        $tanggal_get =  $_POST['tanggal_absen'];
        $sql = "select count(1) AS CNT from dual where TO_DATE(to_char(sysdate,'DD/MM/YYYY'),'DD/MM/YYYY') < TO_DATE('$tanggal_get','DD/MM/YYYY')";
        $result = oci_parse($conn222, $sql);
        oci_execute($result);
        $row = oci_fetch_array($result);
        $do_check = $row['CNT'];
        if($do_check > 0){
            $output = array(
                "message_type"=>1,
                "message"=> "Anda Tidak Bisa Mengajukan Excuse Lebih Dari Hari Ini"
            );
        }else{
            $output = array(
                "message_type"=>2,
                "message"=>"Okay Dude"
            );
        }
        
        echo json_encode($output);
    }
     if(isset($_POST['get_out']) && !empty($_POST['get_out'])) {
        
        $user_id =  $_POST['user_id'];
        $tanggal_absen =  $_POST['tanggal_absen'];

        $sqlchekin = "select COUNT(USER_ID) AS CNT from SIAK_ABSENSI WHERE USER_ID = '$user_id' and to_char(TANGGAL,'DD/MM/YYYY') = '$tanggal_absen'";
        $resultchekin = oci_parse($conn222, $sqlchekin);
        oci_execute($resultchekin);
        $countchekin = oci_fetch_array($resultchekin);
        $do_check = $countchekin['CNT'];
        if($do_check > 0){
            $sql= "select  JAM_MASUK, JAM_KELUAR from SIAK_ABSENSI WHERE USER_ID = '$user_id' and to_char(TANGGAL,'DD/MM/YYYY') = '$tanggal_absen'";
            $result=oci_parse($conn222, $sql);
            oci_execute($result);
            $row = oci_fetch_array($result);
            $clock_in = $row['JAM_MASUK'];
            $clock_out = $row['JAM_KELUAR'];
            $output = array(
                "message_type"=>1,
                "clock_in"=> $clock_in,
                "clock_out"=> $clock_out,
                "message"=> $sqlchekin,
                "sql"=> $sql
            );
        }else{
            $output = array(
                "message_type"=>2,
                "clock_in"=>"00:00",
                "clock_out"=> "00:00",
                "message"=>"Maaf Pada Tanggal $tanggal_absen Anda Tidak Clock-In Atau Clock Out",
                "sql"=> "SQL"  
            );
        }
         echo json_encode($output);
    }
     if(isset($_POST['get_absen']) && !empty($_POST['get_absen'])) {
        
        $user_id =  $_POST['user_id'];
        $tanggal_absen =  $_POST['tanggal_absen'];

        $sqlchekin = "select COUNT(USER_ID) AS CNT from SIAK_ABSENSI WHERE USER_ID = '$user_id' and to_char(TANGGAL,'DD/MM/YYYY') = '$tanggal_absen'";
        $resultchekin = oci_parse($conn222, $sqlchekin);
        oci_execute($resultchekin);
        $countchekin = oci_fetch_array($resultchekin);
        $do_check = $countchekin['CNT'];
        if($do_check > 0){
            $sql= "select  JAM_MASUK, JAM_KELUAR from SIAK_ABSENSI WHERE USER_ID = '$user_id' and to_char(TANGGAL,'DD/MM/YYYY') = '$tanggal_absen'";
            $result=oci_parse($conn222, $sql);
            oci_execute($result);
            $row = oci_fetch_array($result);
            $clock_in = $row['JAM_MASUK'];
            $clock_out = $row['JAM_KELUAR'];
            $output = array(
                "message_type"=>1,
                "clock_in"=> $clock_in,
                "clock_out"=> $clock_out,
                "message"=>"Maaf Pada Tanggal $tanggal_absen Anda Telah Clock-In Atau Clock Out",
                "sql"=> $sql
            );
        }else{
            $output = array(
                "message_type"=>2,
                "clock_in"=>"08:00",
                "clock_out"=> "17:00",
                "message"=>"-",
                "sql"=> "-"  
            );
        }
         echo json_encode($output);
    }
     if(isset($_POST['excuse_save']) && !empty($_POST['excuse_save'])) {
        $user_id =  $_POST['user_id'];
        $nama_lgkp =  $_POST['nama_lgkp'];
        $excuse_tanggal =  $_POST['excuse_tanggal'];
        $excuse_keterangan =  $_POST['excuse_keterangan'];
        $excuse_ci =  $_POST['excuse_ci'];
        $excuse_co =  $_POST['excuse_co'];
        $sql = "SELECT EXCUSE_DESCRIPTION AS DESCRIP FROM SIAK_MASTER_EXCUSE WHERE EXCUSE_ID = $excuse_keterangan";
        $result = oci_parse($conn222, $sql);
        oci_execute($result);
        $row = oci_fetch_array($result);
        $DESCRIP = ucwords(strtolower($row['DESCRIP']));
        if($excuse_keterangan == 1 || $excuse_keterangan == 4 ){
            $sql= "INSERT INTO SIAK_ABSENSI (SEQ_ID,USER_ID, NAMA, TANGGAL, JAM_MASUK, JAM_KELUAR, KETERANGAN, CI_FROM,IP_ADDRESS) VALUES (CONCAT(TO_CHAR(TO_DATE('$excuse_tanggal','DD-MM-YYYY'),'DDMMYYYY'),'-$user_id-excuse-".time()."'),'$user_id','$nama_lgkp',TO_DATE('$excuse_tanggal','DD/MM/YYYY'),'$excuse_ci','$excuse_co','$DESCRIP','WEB','$ipaddress')";
            $result=oci_parse($conn222, $sql);
            oci_execute($result);
            $output = array(
                "message_type"=>1,
                "message"=> "Data Berhasil Di Insert",
                "SQL"=> $sql
            );
        }else if($excuse_keterangan == 2 ||$excuse_keterangan == 3 ){
            $sql= "UPDATE SIAK_ABSENSI A SET A.JAM_MASUK = '$excuse_ci', A.JAM_KELUAR = '$excuse_co', A.KETERANGAN = '$DESCRIP' WHERE A.USER_ID = '$user_id' AND A.TANGGAL = TO_DATE('$excuse_tanggal','DD/MM/YYYY') AND NOT EXISTS (SELECT 1 FROM SIAK_MASTER_EXCUSE B WHERE UPPER(A.KETERANGAN) = B.EXCUSE_DESCRIPTION)";
                $result=oci_parse($conn222, $sql);
                oci_execute($result);
            $output = array(
                "message_type"=>1,
                "message"=> "Data Berhasil Di Update",
                "SQL"=> $sql

            );
        }else{
            $output = array(
                "message_type"=>2,
                "message"=> "Maaf Terjadi Kesalahan"
            );
        }
        echo json_encode($output);
    }
  
    function clean($string) {
   	$string = str_replace(' ', ' ', $string); // Replaces all spaces with hyphens.
   	return preg_replace('/[^A-Za-z0-9\- ,.]/', '', $string); // Removes special chars.
	}
?>