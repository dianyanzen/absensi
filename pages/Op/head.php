<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="description" content="Dinas Kependudukan dan Pencatatan Sipil Kota Bandung">
    <meta name="author" content="Dianyanzen">
    <meta name="keywords" content="Dinas Kependudukan dan Pencatatan Sipil KOTA BANDUNG, Dinas, Kependudukan, Pencatatan Sipil, KOTA BANDUNG, BANDUNG, KTP-el, Kartu Keluarga, KTP, e-KTP, Akta, Akta Kelahiran, Akta Perkawinan, Akta Perceraian, Akta Kematian, Penduduk">
        
    <!--<link rel="shortcut icon" href="http://disdukcapil.bandung.go.id/assets/img/favicon.jpg">-->

    <title>DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL KOTA BANDUNG</title>

    <!-- Le styles -->
	<link rel="shortcut icon" href="../../logo.png" />	
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">
    <link href="assets/css/font-style.css" rel="stylesheet">
    <link href="assets/css/flexslider.css" rel="stylesheet">
    <link href="assets/css/sb-admin-2.css" rel="stylesheet">
    <link href="dist/jquery-clockpicker.css" rel="stylesheet">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.min.css">
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery-latest.js"></script>
	
  <script src="js/toastr.js"></script>
        <script type="text/javascript">
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        </script>
        <link rel="stylesheet" href="css/toastr.css" />

    <style type="text/css">
      body {
        padding-top: 60px;
      }

    </style>


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


  	<!-- Google Fonts call. Font Used Open Sans & Raleway -->

	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">
		function AlertIt() {
		alert("Punten, Programmerna Nuju Sibuk");
		}
</script>
	
<script type="text/javascript">
$(document).ready(function () {

    $("#btn-blog-next").click(function () {
      $('#blogCarousel').carousel('next')
    });
     $("#btn-blog-prev").click(function () {
      $('#blogCarousel').carousel('prev')
    });

     $("#btn-client-next").click(function () {
      $('#clientCarousel').carousel('next')
    });
     $("#btn-client-prev").click(function () {
      $('#clientCarousel').carousel('prev')
    });
    
});

/* $(window).load(function(){

    $('.flexslider').flexslider({
        animation: "slide",
        slideshow: true,
        start: function(slider){
          $('body').removeClass('loading');
        }
    });  
});*/

</script>

	<style>
	#footerwrap {
	   position: fixed !important;
	   left: 0 !important;
	   bottom: 0 !important;
	   width: 100% !important;
	   z-index: -1 !important;
	   // background-color: red;
	   // color: white;
	   text-align: center;
	}
</style>
    
  </head>